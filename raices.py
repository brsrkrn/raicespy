import math

def raices(a,b,c):

    x1 = (-b + math.sqrt(math.pow(b,2)-4*a*c)) / (2*a)
    x2 = (-b - math.sqrt(math.pow(b,2)-4*a*c)) / (2*a)

    return x1,x2


a = input("Ingresa el termino cuadratico ")
b = input("Ingresa el termino lineal ")
c = input("Ingresa el termino independiente ")

x1,x2 = raices(a,b,c)

print "a  =",a
print "b  =",b
print "c  =",c
print "x1 =",x1
print "x2 =",x2
